type tQuestion = bytes
type tAnswer   = signature
type tIdentity = key * tQuestion
type tSignal   = key * tAnswer * tQuestion
type tResource = string
type tDate     = timestamp
type tModification = string
type tDNS_Name = string
type tDNS_IP   = string
type tTzNameStorage =
   { sProtocol    : string
   ; sTzName      : tResource
   ; sAvailable   : bool
   ; sOwner       : tIdentity
   ; sAdmin       : tIdentity
   ; sDNS         : (tDNS_Name, tDNS_IP) map
   ; sAppliedDate      : tDate
   ; sExpireDate       : tDate
   ; sLastModification : tDate * tModification
   ; sRoot             : address (* for demo *)
   }

type storage = tTzNameStorage

let[@inline] oneYear = 31536000
let[@inline] noop = ([] : operation list)
let[@inline] idFromSignal (k, _, q : tSignal) : tIdentity = (k , q)

let checkIdentity
   (sigIn : tSignal) (xK , xQ : tIdentity) : bool =
   let (sigK, sigA, _) = sigIn in
   let k1 = Crypto.hash_key sigK in
   let k2 = Crypto.hash_key xK in
   (k1 = k2) && (Crypto.check xK sigA xQ)

(* ↓↓↓↓↓↓↓↓↓↓ entry ↓↓↓↓↓↓↓↓↓↓ *)

let%entry register ( sigIn , newOwner : tSignal * tIdentity ) s =
   if (not s.sAvailable)
   then Current.failwith "This TzName is not available right now.";
   let b = checkIdentity sigIn s.sAdmin in
   if b then
      let timeNow = Current.time () in
      let timeEnd = timeNow + oneYear in
      let s = s.sAppliedDate <- timeNow in
      let s = s.sExpireDate  <- timeEnd in
      let s = s.sAvailable <- false in
      let s = s.sOwner <- newOwner in
      let s = s.sAdmin <- idFromSignal sigIn in
      let s = s.sLastModification <- (timeNow, "registered") in
      (noop , s)
   else (noop , s)

let%entry renew ( sigIn : tSignal ) s =
   if (s.sAvailable)
   then Current.failwith "This TzName is not renewable. Plz register again.";
   let b = checkIdentity sigIn s.sOwner in
   if b then
      let timeNow = Current.time () in
      let timeEnd = s.sExpireDate + oneYear in
      let s = s.sExpireDate  <- timeEnd in
      let s = s.sAvailable <- false in
      let s = s.sOwner <- idFromSignal sigIn in
      let s = s.sLastModification <- (timeNow, "renewed") in
      (noop , s)
   else (noop , s)

let%entry free ( sigIn : tSignal ) s =
   let b = checkIdentity sigIn s.sAdmin in
   if b then
      let timeNow = Current.time () in
      let s = s.sLastModification <- (timeNow, "released") in
      let s = s.sAdmin <- idFromSignal sigIn in
      let s = s.sAvailable <- true in
      noop , s
   else (noop , s)

(* ↓↓↓↓↓↓↓↓↓↓ backdoor for demo ↓↓↓↓↓↓↓↓↓↓ *)

let[@inline] errInfo s = String.concat ["["; s.sProtocol; "] "; "invalid sender"]
let[@inline] guardSendFromAdmin s =
   let sender = Current.sender () in
   if sender <> s.sRoot then Current.failwith (errInfo s)

(* for demo *)
let%entry updateRoot (newRoot : address) s =
   guardSendFromAdmin s;
   let s = s.sRoot <- newRoot in
   (noop , s)

(* for demo *)
let%entry updateAdmin (newAdmin : tIdentity) s =
   guardSendFromAdmin s;
   let s = s.sAdmin <- newAdmin in
   (noop , s)
