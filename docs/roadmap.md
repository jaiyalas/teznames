!!! info "roadmap and release plan"

    + The end of **June** 2019
        - get users guide (for _9chsTNS-0.0.2_) ready
        - support anonymous (hashed) name
    + The 1st half of **July** 2019
        - support restricted name list
        - support premium name list
    + The 2st half of **July** 2019
        - support auction (or taxing)
        - support sub-domain deployment
    + August 2019
        - TBA
    + September 2019
        - TBA

!!! check "todo"

    + [x] [doc] contribute notes
    + [x] [doc] add explanations of system key components
    + [x] [doc] add contents on page _system/\*_
    + [ ] [doc] add contents on page _Use case/\*_
    + [ ] [release] review and confirm coverage of all documentations
    + [ ] [update] plain name → hashed name
    + [ ] [update] protocol checking on cTNRecords
    + [ ] [update] cTezName auto update to cTNRecords
    + [ ] [update] inquire info/storage on TNS/TNRS
        + [x] by contract alias
        + [ ] by KT1 address
