<img src="img/teznames_logo.jpg" alt="logo" width="250"/>

The [**teznames**](https://teznames.readthedocs.io/en/latest/system/overview/) is a name registration service on and for tezos blockchain. It provides [contracts](https://teznames.readthedocs.io/en/latest/system/spec/) as the definition of _tezname_.

The _teznames defines_ no client-side behavior. Which means you can just think _tezname_ as a normal tezos contract and use _tezos-client_ to interact with. However we highly recommand to use the [**dough**](https://gitlab.com/9chapters/dough), a commandline tool developed by the [**Nine Chapters**](https://www.9chapters.io/) for interact with _teznames_. You can find more details in [source code](https://gitlab.com/9chapters/dough) and [users guide](https://dough.readthedocs.io/en/latest/).
