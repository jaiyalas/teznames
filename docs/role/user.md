!!! info "9chsTNS-0.0.1"

## inquire a tezname

To inquire whois-ish info of a tezname is simple. It's basically a public information on contract so any contract explorer can be helpful.

!!! hint "Using **dough**!"

    The _dough_ also provide a command `dough inquire` that can inquire and show on screen. [Check this document!](https://dough.readthedocs.io/en/latest/cmd/tezname/#command-inquire)

    !!! todo

        Using _asciinema_ to record `dough inquire` and update/insert into here.

## originate a tezname

As a regular user, you can naturally become a _tezname owner_ by deploying your own _tezname contract_. You can originate `cTezName.tz` by directly using `tezos-client`.

!!! hint "Using **dough**!"

    There are two ways to use _dough_ for deploying a cTezName.

    The one way is using `dough data` ([users guide](https://dough.readthedocs.io/en/latest/cmd/tezname/#command-data)) to prepare init data and manually originate cTezName via _tezos-client_.

    The other way is using `dough deploy` ([users guide](https://dough.readthedocs.io/en/latest/cmd/tezname/#command-deploy)) to deploy cTezName via _dough_ rather than _tezos-client_. All you need to do is to follow the following steps.

    **Step 1**: Prepare a record data file as follow. The dough will take this data as Haskell record data structure and translate it into Michelson format.

    ```haskell
    TzNameData
       { sProtocol  = "9chsTNS-0.0.1"
       , sTzName    = "mico"
       , sAvailable = "False"
       , sOwner = TNID "abpH"
       , sAdmin = TNID "dotblack"
       , sDNS =
          [ ("dns1.tzname.org", "192.168.1.100")
          , ("dns2.tzname.org", "192.168.1.103") ]
       , sAppliedDate      =  "2019-01-01T04:30:00Z"
       , sExpireDate       =  "2020-01-01T04:30:01Z"
       , sLastModification = ("2019-01-01T04:30:00Z", "originated")
       , sRoot = "tz1bhXKVY4ihH8Dcao4cuk8KxJ4sPjXGZcEp"
       }
    ```

    **Step 2**: Exec `dough deploy`.

    Say, you want to register `mico.tez` with `./test/admin/initA.data` as init data file and `./src/9chsTNS-0.0.1/cTezName.tz` as the source tezname contract. All you need to do is exec the following instruction:

    ```bash
    dough deploy -c dough.config -d ./test/admin/initA.data -i ./src/9chsTNS-0.0.1/cTezName.tz -n mico.tez
    ```

    The returned info will be displayed on terminal. It shows the _tezname_, the _source code of cTezName_, the _originated address_ and the address of tezname records contract.

    ```bash
    [INFO]
         TzName contract:mico.tez of ./src/9chsTNS-0.0.1/cTezName.tz
         has been deploied successfully at address: KT1ATUkZPKuMsF9WL7moaBgeTW6iB4gpPYBj
    [INFO]
         This origination record has been updated onto KT1P4fir3MtexzztWbybNxonCv28Fu6hwmGA
    ```

    !!! todo

        Using _asciinema_ to record `dough inquire` and update/insert into here.

After you deploy your tezname contract, you can checking with the [instance of tezname record contract](https://baking-bad.github.io/better-call-dev/#alpha:KT1P4fir3MtexzztWbybNxonCv28Fu6hwmGA).
