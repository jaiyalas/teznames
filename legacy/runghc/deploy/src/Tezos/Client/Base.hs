{-# LANGUAGE OverloadedStrings #-}
--
module Tezos.Client.Base where
--
import Shelly
--
import qualified Data.Text as T
--
runTz :: [T.Text] -> Sh T.Text
runTz ts = run "tezos-client" ts
--
alphaNode :: [T.Text]
alphaNode = ["-A", "node1.lax.tezos.org.sg"]
--
-- tezos-client -A node1.lax.tezos.org.sg get script storage for cTzName
