module Tezos.Client.CmdArg where
--
-- import Shelly (echo, inspect)
--
import Options.Applicative
import Data.Semigroup ((<>))
--
data Command
  = Misc MiscOpt
  | ShowKnownTz Bool
  | Register RegisterOpt
  | Inquiry InquiryOpt
  deriving (Show, Eq)
--
data MiscOpt = MiscOpt
   { inFile :: String
   , outFile :: String
   }
   deriving (Show, Eq)
--
opts :: ParserInfo Command
opts = info (args <**> helper) idm
--
args :: Parser Command
args = subparser
   (  cmdMisc
   <> cmdTZ
   <> cmdRegister
   <> cmdInquiry
   )
--
cmds f = foldl1 (<>) . map ((flip command) f)
--
cmdMisc = cmds (info miscParser (progDesc "Misc"))
   ["misc"]
cmdTZ = cmds (info tzParser (progDesc "Show all SK known accounts"))
   ["tz", "wallet"]
cmdRegister = cmds (info registerParser (progDesc "Show all known contracts"))
   ["reg", "register", "deploy"]
cmdInquiry = cmds (info inquiryParser (progDesc "Show all known contracts"))
   ["inquiry", "ask", "query", "name"]
--
-- >>>>>>>>>> CMD PARSER <<<<<<<<<<
--
miscParser :: Parser Command
miscParser =  Misc <$> miscOptParser
--
tzParser :: Parser Command
tzParser = ShowKnownTz <$> tzOptParser
--
registerParser :: Parser Command
registerParser = Register <$> registerOptParser
--
inquiryParser :: Parser Command
inquiryParser = Inquiry <$> inquiryOptParser
--
-- >>>>>>>>>> OPT PARSER <<<<<<<<<<
--
data RegisterOpt =
   RegisterOpt String
   deriving (Show, Eq)
--
data InquiryOpt
   = InquiryOpt String
   deriving (Show, Eq)
--
inquiryOptParser :: Parser InquiryOpt
inquiryOptParser = InquiryOpt <$>
   (addressParser <|> keyParser)
--
registerOptParser :: Parser RegisterOpt
registerOptParser = RegisterOpt <$> nameParser
--
nameParser = strOption
  (  long "name"
  <> short 'n'
  <> metavar "NAME"
  <> help "terget name" )
addressParser = strOption
  (  long "addr"
  <> short 'a'
  <> metavar "KT-ADDR"
  <> help "KT address" )
keyParser = strOption
  (  long "edp-key"
  <> short 'k'
  <> metavar "edp-KEY"
  <> help "Key" )
--
tzOptParser :: Parser Bool
tzOptParser = switch
   ( long "right"
   <> short 'r'
   <> help "Align account name to right" )
--
miscOptParser :: Parser MiscOpt
miscOptParser = MiscOpt
   <$> inFileParser
   <*> outFileParser
--
inFileParser :: Parser String
inFileParser = strOption
  (  long "infile"
  <> short 'i'
  <> metavar "FILE"
  <> help "Target for the greeting" )
--
outFileParser :: Parser String
outFileParser = strOption
  (  long "outfile"
  <> short 'o'
  <> metavar "FILE"
  <> value "log.out"
  <> help "Target for the greeting" )
--

--
--
-- sample :: Parser Sample
-- sample = Sample
--       <$> strOption
--           ( long "hello"
--          <> metavar "TARGET"
--          <> help "Target for the greeting" )
--       <*> switch
--           ( long "quiet"
--          <> short 'q'
--          <> help "Whether to be quiet" )
--       <*> option auto
--           ( long "enthusiasm"
--          <> help "How enthusiastically to greet"
--          <> showDefault
--          <> value 1
--          <> metavar "INT" )
