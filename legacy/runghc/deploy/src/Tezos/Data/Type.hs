module Tezos.Data.Type where
--
import qualified Data.Text as T
--
data MiType
   = MiType_Address
   | MiType_Bytes
   | MiType_Key
   | MiType_KeyHash
   | MiType_Signature
   | MiType_String -- timestamp included
   | MiType_Unit
   | MiType_Num
   | MiType_Bool
   deriving (Show, Eq)
--

data Expr a
   = Nest [Expr a]
   | Atom a
   deriving (Show, Eq, Read)
--
