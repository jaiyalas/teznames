{-# LANGUAGE OverloadedStrings #-}
--
module Tezos.Data.StringCast where
--
import Tezos.Data.Type
--
import Data.Char (isDigit)
import qualified Data.Text as T
--
class MiData a where
   attachType :: a -> (MiType, a)
   toMichelsonFormat :: MiType -> a -> a
-- class MiFunctor f where
--    attachType :: (MiData a) => f a -> (MiType, f a)
-- class MiBiFunctor b where
--    attachType :: (MiData a, MiData b) => f a b -> (MiType, f a b)
-- --
-- instance MiFunctor [] where
--    attachType [] = (MiType_Unit, ())
--    attachType xs = (MiType_List, xs)
--
-- instance MiFunctor Maybe where
--    attachType t = (MiType_Maybe, t)
-- --
-- instance MiBiFunctor (,) where
--    attachType (a, b) = undefined
-- instance MiBiFunctor Either where
--    attachType = undefined
--
instance MiData Bool where
   attachType t = (MiType_Bool, t)
   toMichelsonFormat _ = id

instance MiData Int where
   attachType t = (MiType_Num, t)
   toMichelsonFormat _ = id
instance MiData () where
   attachType () = (MiType_Unit, ())
   toMichelsonFormat _ = id
--
instance MiData T.Text where
   attachType t
      | isTZ1     t = (MiType_Address, t)
      | isKT1     t = (MiType_Address, t)
      | isBytes   t = (MiType_Bytes, t)
      | isKey     t = (MiType_Key, t)
      | isKeyHash t = (MiType_KeyHash, t)
      | isSig     t = (MiType_Signature, t)
      | isBool    t = (MiType_Bool, t)
      | isUnit    t = (MiType_Unit, t)
      | isNum     t = (MiType_Num, t)
      | otherwise   = (MiType_String, t)
   --
   toMichelsonFormat MiType_Address   t = T.concat ["\"", t, "\""]
   toMichelsonFormat MiType_Key       t = T.concat ["\"", t, "\""]
   toMichelsonFormat MiType_KeyHash   t = T.concat ["\"", t, "\""]
   toMichelsonFormat MiType_Signature t = T.concat ["\"", t, "\""]
   toMichelsonFormat MiType_String    t = T.concat ["\"", t, "\""]
   toMichelsonFormat MiType_Bytes     t = t
   toMichelsonFormat MiType_Bool      t = t
   toMichelsonFormat MiType_Num       t = t
   toMichelsonFormat MiType_Unit      t = t
   --
--
isTZ1     t = T.isPrefixOf "tz1" t
isKT1     t = T.isPrefixOf "KT1" t
isBytes   t = T.isPrefixOf "0x" t
isKey     t = T.isPrefixOf "edp" t
isKeyHash t = T.isPrefixOf "tz1" t
isSig     t = T.isPrefixOf "edsig" t
isBool    t = (T.isPrefixOf "false" t) || T.isPrefixOf "true" t
isUnit    t = T.isPrefixOf "unit" t
isNum     t = and $ map isDigit $ T.unpack t
--


--
ppString :: (MiData a) => a -> a
ppString = (uncurry toMichelsonFormat) . attachType
--
ppSS :: (Expr T.Text) -> T.Text
ppSS (Atom t) = ppString t
ppSS (Nest []) = ""
ppSS (Nest [t]) = ppSS t
ppSS (Nest (t:ts)) = T.concat ["(Pair ", ppSS t, " ", ppSS (Nest ts), ")"]
--
