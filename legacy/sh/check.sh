#!/bin/bash

dataSet=(\'\"tz1bhXKVY4ihH8Dcao4cuk8KxJ4sPjXGZcEp\"\') # '"tz1fjcqKic5pHzFLhUxWBpiSe8KA77jve1TD"' '"edpkuesJWtpYgjM1wjxHn7oiJsT64rMrGXztoa7eQ9MGRCL2GtEm21"' '"edsk35ZbujnCeZJ9dfnk9H95tsS3KC747vX8pyBKTrGa9BDrX2UJBh"')
typeSet=(address, key, key_hash)



# tezos-client -A node1.lax.tezos.org.sg typecheck data '"$data"' against type address

for data in ${dataSet[*]}
do
   for type in ${typeSet[*]}
   do
      echo "tezos-client -A node1.lax.tezos.org.sg typecheck data $data against type $type"
      tezos-client -A node1.lax.tezos.org.sg typecheck data $data against type $type
      echo '-----'
   done
   echo ''
done
