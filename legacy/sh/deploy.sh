#!/bin/bash

alphaNode=node1.lax.tezos.org.sg
contractName=cTzName
origin=alias:dotblack
money=1
FILE=cTzName.tz

DATA='(Pair "kingcaleb" (Pair (Pair "edpkuecuybjhNpjDwoZKXg3FrrJBmSEz5FsGQBdo7yCjHnfRs1tpDd" 0x05010000000a7175657374696f6e3030) (Pair (Pair "edpkuesJWtpYgjM1wjxHn7oiJsT64rMrGXztoa7eQ9MGRCL2GtEm21" 0x05010000000a7175657374696f6e3030) (Pair "2019-01-01T10:01:00+01:00" (Pair "2020-01-01T10:01:00+01:00" (Pair (Pair "2015-05-22T10:01:00+01:00" "ORIGINATED") (Pair {"(name1.dns, 192.168.1.1)"; "(name2.dns, 192.168.1.2)"} False)))))))'

DATB='(Pair "caleb.net" (Pair (Pair "edpkvDAJ17819iBCCSq4bKZNFvdrJSwC6HAv5WrC8mxecup5kzP3cM" 0x05010000000a7175657374696f6e3030) (Pair (Pair "edpkuesJWtpYgjM1wjxHn7oiJsT64rMrGXztoa7eQ9MGRCL2GtEm21" 0x05010000000a7175657374696f6e3030) (Pair "01/01/1970, 08:33:39" (Pair "01/01/1970, 08:33:40" (Pair "01/01/1970, 08:33:39" (Pair "register" (Pair (Pair "192.168.1.1" "192.168.1.2") false))))))))'

if [ "$2" != "" ];
then
   tezos-client -A $alphaNode originate contract $contractName for $origin transferring $money from $origin running $FILE --init "$DATA" --burn-cap $1 -q $2
else
   tezos-client -A $alphaNode originate contract $contractName for $origin transferring $money from $origin running $FILE --init "$DATA" --burn-cap $1 -q
fi




# 20:39 ~/.tezos-client > tezos-client -A node1.lax.tezos.org.sg show address dotblack -S
   # Hash: tz1bhXKVY4ihH8Dcao4cuk8KxJ4sPjXGZcEp
   # Public Key: edpkuesJWtpYgjM1wjxHn7oiJsT64rMrGXztoa7eQ9MGRCL2GtEm21
   # Secret Key: unencrypted:edsk35ZbujnCeZJ9dfnk9H95tsS3KC747vX8pyBKTrGa9BDrX2UJBh
# 20:40 ~/.tezos-client > tezos-client -A node1.lax.tezos.org.sg show address abpS -S
   # Hash: tz1fjcqKic5pHzFLhUxWBpiSe8KA77jve1TD
   # Public Key: edpkuecuybjhNpjDwoZKXg3FrrJBmSEz5FsGQBdo7yCjHnfRs1tpDd
   # Secret Key: unencrypted:edsk4XYVxjGXvYS7nNJUrry4BiFDuig4KDRrCu4KJHVxCVqV43p1NN
