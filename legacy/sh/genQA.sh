#!/bin/bash

NAMES=(abpH abpS dotblack)

for NAME in ${NAMES[*]}
do
   echo $NAME
   tezos-client -A node1.lax.tezos.org.sg sign bytes $1 for $NAME
done
