#!/bin/bash


# dotblack:
#    tz1bhXKVY4ihH8Dcao4cuk8KxJ4sPjXGZcEp
#    edpkuesJWtpYgjM1wjxHn7oiJsT64rMrGXztoa7eQ9MGRCL2GtEm21
# abpS:
#    tz1fjcqKic5pHzFLhUxWBpiSe8KA77jve1TD
#    edpkuecuybjhNpjDwoZKXg3FrrJBmSEz5FsGQBdo7yCjHnfRs1tpDd

# { sProtocol    : string
# ; sTzName      : tResource
# ; sAvailable   : bool
# ; sOwner       : tIdentity
# ; sAdmin       : tIdentity
# ; sDNS         : (tDNS_Name, tDNS_IP) map
# ; sAppliedDate      : tDate
# ; sExpireDate       : tDate
# ; sLastModification : tDate * tModification
# ; sRoot             : address (* for demo *)
# }

#
# sProtocol    = "9chsTNRS-0.0.1" # : string
# sTzName      = "mico" # : tResource
# sAvailable   = False # : bool
# sOwner       = (Pair "" ) # : (string, bytes)
# sAdmin       = # : (string, bytes)
# sDNS         = # : (tDNS_Name, tDNS_IP) map
# sAppliedDate      = # : tDate
# sExpireDate       = # : tDate
# sLastModification = # : tDate * tModification
# sRoot             = # : address
#

PARA='Left (Pair (Pair "edpkuesJWtpYgjM1wjxHn7oiJsT64rMrGXztoa7eQ9MGRCL2GtEm21" (Pair "edsigu4ZseTD1AhrpSmiUK9U5JsHLtjXGN1FfUL2QQbxmZBxc6QnDPfNMerZBcQVWEosHRzWut6rLYxK3kRD51hxe4sQVFZMEe1"  0x05010000000a7175657374696f6e3032)) (Pair "edpkvDAJ17819iBCCSq4bKZNFvdrJSwC6HAv5WrC8mxecup5kzP3cM" 0x05010000000a7175657374696f6e3031))'

contractName=cTzName
origin=alias:dotblack

if [ "$2" != "" ];
then
   tezos-client -A node1.lax.tezos.org.sg transfer 0 from $origin to $contractName --arg "$PARA" -q --burn-cap $1 $2
else
   tezos-client -A node1.lax.tezos.org.sg transfer 0 from $origin to $contractName --arg "$PARA" -q --burn-cap $1
fi
