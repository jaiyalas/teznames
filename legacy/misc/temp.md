
```
> tezos-client -A node1.lax.tezos.org.sg hash data '"For the Caleb!"' of type string
Raw packed data: 0x05010000000e466f72207468652043616c656221
Hash: exprtmULpXapDpokyuCPSfHRYRc1DG2121pBDB8yJi1VKL7GWbByRW
Raw Blake2b hash: 0x21231a55c5513ae0e6aed4e1b74c19cca34e3a87ec37df39e3867a8d473f2576
Raw Sha256 hash: 0x2e38c62e706c6ef6356f049ad9823324c0093705c4be5bf75a30f0d929aa32fb
Raw Sha512 hash: 0x9963c193ccd6a634cf0e5789f3cce9774d11c7e6fc775d82d3ef3d5fcacb5c4861623cbd7cb22c42fc19aad0606f49a22f823eca34b22b9abbd99300c07f0d74
Gas remaining: 399910 units remaining
```

### ..

dotblack
   edpkuesJWtpYgjM1wjxHn7oiJsT64rMrGXztoa7eQ9MGRCL2GtEm21
```
'(Pair (Pair "edpkuesJWtpYgjM1wjxHn7oiJsT64rMrGXztoa7eQ9MGRCL2GtEm21" 0x05010000000e466f72207468652043616c656221) {})'
```

imm-0.1 deployed at
   KT1TWutbeeFWSf1Ydxrrmf9UNk2seMRBKGb6

dotblack's ans
   edsigtwXQQAtfNxDkCFohjneGGL7UJ2XmjKyUfKbwh3DugWG8WAzsy7f6hdp8mVcHzNCts3vbd9teXVRyjyfaJk34jLm2r4kY8b

### ..

abpS
   edpkuecuybjhNpjDwoZKXg3FrrJBmSEz5FsGQBdo7yCjHnfRs1tpDd
```
'(Pair (Pair "edpkuecuybjhNpjDwoZKXg3FrrJBmSEz5FsGQBdo7yCjHnfRs1tpDd" 0x05010000000e466f72207468652043616c656221) (Pair "edsigtwXQQAtfNxDkCFohjneGGL7UJ2XmjKyUfKbwh3DugWG8WAzsy7f6hdp8mVcHzNCts3vbd9teXVRyjyfaJk34jLm2r4kY8b" "yunyan"))'
```

KT1KQn2mxZ8su4Rfrp656K4vkDUjpECTU2Ze
